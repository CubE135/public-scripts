#!/bin/bash

echo -e "[Webhook]: Sending webhook to Discord...\\n";

case $1 in
  "dev" )
    EMBED_COLOR=3188479
    ENVIRONMENT_NAME="Dev"
    ;;

  "prod" )
    EMBED_COLOR=16735156
    ENVIRONMENT_NAME="Live"
    ;;

  * )
    EMBED_COLOR=6908265
    ENVIRONMENT_NAME="Unkown"
    ;;
esac

WEBHOOK_DATA='{
    "embeds": [{
        "title": "'"$CI_PROJECT_NAME"'",
        "description": "The Repository has been deployed to the '$ENVIRONMENT_NAME' Environment.",
        "url": "'"$CI_PROJECT_URL"'",
        "color": '$EMBED_COLOR',
        "thumbnail": {
            "url": "'"$GITLAB_LOGO"'"
        },
        "fields": [
            {
                "name": "Pipeline",
                "value": "'"[\`$CI_JOB_STAGE\`]($CI_JOB_URL)"'",
                "inline": true
            },
            {
                "name": "Branch",
                "value": "'"[\`$CI_COMMIT_REF_NAME\`]($CI_PROJECT_URL/tree/$CI_COMMIT_REF_NAME)"'",
                "inline": true
            },
            {
                "name": "Commit",
                "value": "'"[\`$CI_COMMIT_SHORT_SHA\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
                "inline": true
            },
            {
                "name": "Commit Message",
                "value": "'"$CI_COMMIT_MESSAGE"'"
            },
            {
                "name": "Executed By",
                "value": "'"$GITLAB_USER_NAME"' ('"$GITLAB_USER_EMAIL"')"
            }
        ]
    }]
}'

echo $WEBHOOK_DATA

(curl --location --request POST "$WEBHOOK_URL" \
--header 'Content-Type: application/json' \
--data-raw "$WEBHOOK_DATA" \
 && echo -e "\\n[Webhook]: Successfully sent the webhook.") || echo -e "\\n[Webhook]: Unable to send webhook."